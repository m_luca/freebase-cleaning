import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.SparkConf;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Kmean {

    public static void main(String[] args) {

        SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("K-means");
        JavaSparkContext sc = new JavaSparkContext(conf);

        final String unfiltered = "files/freebase-rdf-latest.gz";
        final String regex = ".*\\t/user.*|"
                + ".*\\t/freebase/(?!domain_category).*|"
                + ".*/usergroup/.*|"
                + ".*/permission/.*|"
                + ".*\\t/community/.*\\t.*|"
                + ".*\\t/type/object/type\\t.*|"
                + ".*\\t/type/domain/.*\\t.*|"
                + ".*\\t/type/property/(?!expected_type|reverse_property)\\b.*|"
                + ".*\\t/type/(user|content|attribution|extension|link|namespace|permission|reflect|em|karen|cfs|media).*|"
                + ".*\\t/common/(?!document|topic)\\b.*|"
                + ".*\\t/common/document/(?!source_uri)\\b.*|"
                + ".*\\t/common/topic/(description|image|webpage|properties|weblink|notable_for|article).*|"
                + ".*\\t/type/type/(?!domain|instance)\\b.*|"
                + ".*\\t/dataworld/.*\\t.*|"
                + ".*\\t/base/.*\\t.*";


        JavaRDD<?> input = sc.textFile(unfiltered);

        Pattern rx = Pattern.compile(regex);

        input.filter((s) -> {
            Matcher m = rx.matcher((String) s);
            return !m.matches();
        }).repartition(1).saveAsTextFile("files/filtered_output.txt");

        // Load and parse data
        String path = "files/filtered_output.txt";
        JavaRDD<String> data = sc.textFile(path);
        JavaRDD<Vector> parsedData = data.map(
                new Function<String, Vector>() {
                    public Vector call(String s) {
                        String[] sarray = s.split(" ");
                        double[] values = new double[sarray.length];
                        for (int i = 0; i < sarray.length; i++)
                            values[i] = Double.parseDouble(sarray[i]);
                        return Vectors.dense(values);
                    }
                }
        );
        parsedData.cache();
        int numClusters = 4;
        int numIterations = 20;
        KMeansModel clusters = KMeans.train(parsedData.rdd(), numClusters, numIterations);
        // Evaluate with WSSSE
        double WSSSE = clusters.computeCost(parsedData.rdd());
    }
}
