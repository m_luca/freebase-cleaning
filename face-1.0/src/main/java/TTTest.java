import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TTTest {
    final String path = "files/freebase-rdf-latest.gz";
    static String regex = ".*\\t/user.*|"
            + ".*\\t/freebase/(?!domain_category).*|"
            + ".*/usergroup/.*|"
            + ".*/permission/.*|"
            + ".*\\t/community/.*\\t.*|"
            + ".*\\t/type/object/type\\t.*|"
            + ".*\\t/type/domain/.*\\t.*|"
            + ".*\\t/type/property/(?!expected_type|reverse_property)\\b.*|"
            + ".*\\t/type/(user|content|attribution|extension|link|namespace|permission|reflect|em|karen|cfs|media).*|"
            + ".*\\t/common/(?!document|topic)\\b.*|"
            + ".*\\t/common/document/(?!source_uri)\\b.*|"
            + ".*\\t/common/topic/(description|image|webpage|properties|weblink|notable_for|article).*|"
            + ".*\\t/type/type/(?!domain|instance)\\b.*|"
            + ".*\\t/dataworld/.*\\t.*|"
            + ".*\\t/base/.*\\t.*";


    SparkConf sparkConf = new SparkConf()
            .setAppName("Freebase Reader")
            .setMaster("local[*]")
            .set("spark.executor.memory","2g");
    JavaSparkContext sc = new JavaSparkContext(sparkConf);

    /**
     *
     * @return lines: a Java RDD containing only the core entities
     */
    public void filteredRDD(){

        JavaSparkContext sc = new JavaSparkContext("local[*]", "TTTest");
        JavaRDD<?> input = sc.textFile(path);

        Pattern rx = Pattern.compile(regex);

        input.filter((s) -> {
            Matcher m = rx.matcher((String) s);
            return !m.matches();
        }).repartition(1).saveAsTextFile("files/filtered_output");

        final String filtered = "files/filtered_output";
        Map<String, Map<String, Map <String, Double>>> instances_list = new HashMap<>();
        Map<String, ArrayList<Double>> property_names = new HashMap<>();
        ArrayList<String> to_be_removed = new ArrayList<>();
        // First read, find instances and create the properties/relation array
        FileReader fr = null;
        try {
            fr = new FileReader(filtered);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader br = new BufferedReader(fr);
        String s = null;
        try {
            while ((s = br.readLine()) != null) {
                if (!s.matches(regex)) {
                    String[] a = s.split("\t");
                    if (a[1].equals("<http://rdf.freebase.com/ns/type.type.instance>")) {
                        Map object = new HashMap();
                        Map properties = new HashMap<String, Double>();
                        properties.put(null, null);
                        object.put(a[2], properties);
                        instances_list.put(a[0], object);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileReader fr2 = null;
        try {
            fr2 = new FileReader(filtered);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader br2 = new BufferedReader(fr2);
        String s2 = null;
        try {
            while ((s2 = br2.readLine()) != null) {
                if (!s2.matches(regex)) {
                    String[] a = s2.split("\t");
                    for (String key : instances_list.keySet()) {
                        if( key.equals(a[0])){
                            Map<String, Map<String, Double>> temp_entry_level = instances_list.get(key);
                            for (String name : temp_entry_level.keySet()){
                                Map<String, Double> temp_property_level = temp_entry_level.get(name);
                                if (temp_property_level.containsKey(a[1])){
                                    temp_property_level.put(a[1], temp_property_level.get(a[1]) + 1);
                                }else{
                                    temp_property_level.put(a[1], 1.0);
                                    property_names.put(a[1], new ArrayList<Double>());
                                }
                            }
                            for (String name : temp_entry_level.keySet()){
                                Map<String, Double> temp_property_level = temp_entry_level.get(name);
                                for (String property : property_names.keySet()){
                                    if(temp_property_level.containsKey(property)){
                                        property_names.get(property).add(temp_property_level.get(property));
                                    }else{
                                        property_names.get(property).add(null);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Float average;
        Double sd;
        for (String key : instances_list.keySet()) {
            Map<String, Map<String, Double>> temp_entry_level = instances_list.get(key);
            for (String name : temp_entry_level.keySet()){
                for (String property : property_names.keySet()){
                    ArrayList<Double> support = property_names.get(property);
                    Iterator it = support.iterator();
                    Double partial_sum = 0.0;
                    Integer valid_element = 0;
                    while (it.hasNext()) {
                        Double partial = (Double) it.next();
                        if (partial != null) {
                            valid_element++;
                            partial_sum += partial;
                        }
                    }
                    average = partial_sum.floatValue() / valid_element.floatValue();
                    Iterator it_sd = support.iterator();
                    Double temp_square = 0.0;
                    while (it_sd.hasNext()) {
                        Double root_difference = Math.pow((Double) it_sd.next() - average, 2);
                        temp_square += root_difference;
                    }
                    Double means_diff = temp_square.doubleValue() / valid_element.doubleValue();
                    sd = Math.sqrt(means_diff);
                    property_names.get(property).clear();
                    property_names.get(property).add(average.doubleValue());
                    property_names.get(property).add(sd.doubleValue());
                }
            }
        }


        for (String key : instances_list.keySet()) {
            Map<String, Map<String, Double>> temp_entry_level = instances_list.get(key);
            for (String name : temp_entry_level.keySet()){
                Map<String, Double> temp_property_level = temp_entry_level.get(name);
                for (String property : temp_property_level.keySet()){
                    if(property_names.get(property) != null){
                        if (Math.abs(temp_property_level.get(property) - property_names.get(property).get(0)) < (2 * property_names.get(property).get(1))){
                            // outlier e costruisco la riga da rimuovere sul dataset
                            to_be_removed.add(key+"\t"+name+"\t"+property);
                        }
                    }
                }
            }
        }
        stampaToBeRemoved(to_be_removed);
        sc.stop();
    }


    void stampaToBeRemoved(ArrayList<String> a){
        Iterator it = a.iterator();

        Path path = Paths.get("files/toberemoved.txt");

        while(it.hasNext()){
            try {
                Files.write(path,((String)it.next()).getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args){
        TTTest reader = new TTTest();
        reader.filteredRDD();
    }

}
