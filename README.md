# Freebase-Cleaning

In both the folders, a Dockerfile file can be found in order to run the code.

**Before running everything, download a data dump from https://developers.google.com/freebase/ and put it in the folder files**


To run everything, use

docker run -v <path>/face-1.0/files:/files --name=<name> -t <whatever>
docker build -t <whatever>